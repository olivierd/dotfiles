# .bashrc
#

set -o emacs

if [ `id -un` != root ]; then
	PS1="\u@\h:\w \$ "
else
	PS1="\[\033[0;31m\]\u@\h:\w #\[\033[0m\] "
fi

if [ -z "$CLICOLOR" ]; then
	export CLICOLOR="--color=always"
fi
alias ls="/bin/ls $CLICOLOR"
