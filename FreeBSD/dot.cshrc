# $FreeBSD: release/10.0.0/etc/root/dot.cshrc 243893 2012-12-05 13:56:39Z eadler $
#
# .cshrc - csh resource script, read at beginning of execution by each shell
#
# see also csh(1), environ(7).
# more examples available at /usr/share/examples/csh/
#

# A righteous umask
umask 22

set path = (/bin /usr/sbin /usr/bin /usr/local/sbin /usr/local/bin $HOME/bin)

setenv	EDITOR	vi
setenv	PAGER	less
setenv	BLOCKSIZE	K
setenv	CLICOLOR	enable
setenv	LSCOLORS	exfxbxdxcxegedabagacad

if ($?prompt) then
	# An interactive shell -- set some stuff up
	set prompt = "%n@%m:%~ %# "

	set filec
	set history = 30
	set savehist = (30 merge)
	set autolist = ambiguous
	# Use history to aid expansion
	set autoexpand
	set autorehash
	set mail = (/var/mail/$USER)
	if ( $?tcsh ) then
		bindkey "^W" backward-delete-word
		bindkey -k up history-search-backward
		bindkey -k down history-search-forward
	endif

endif
