;; .emacs.el
;; -*- coding: iso-8859-15 -*-
;;

;; Buffer *scratch* vide au lancement de Emacs
(setq inhibit-startup-message t
	inhibit-startup-echo-area-message t)

;; Suppression de la barre d'outils (celle avec les icones)
(tool-bar-mode 0)
(menu-bar-mode nil)

;; Suppression de la � scroll � barre
(scroll-bar-mode 0)

;; Emp�che le clignotement du curseur
(blink-cursor-mode 0)

;; D�sactive la sauvegarde des fichiers (avec l'extension ~)
(setq make-backup-files nil)

;; Affiche le num�ro de la ligne
(line-number-mode t)

;; Affiche le num�ro de colonne
(column-number-mode t)

;; Affiche la taille du fichier (celui du buffer courant)
(size-indication-mode t)

;; Le contenu se d�place d'une seule ligne
(setq scroll-step 1)

;; Lignes ne d�passant pas 76 colonnes
(setq-default fill-column 76)

;; Tabulation (longueur)
(setq-default tab-width 2)

;; Active la coloration syntaxique
(global-font-lock-mode t)
(setq-default font-lock-maximum-decoration t)

;; Mode majeur text-mode
(setq default-major-mode 'text-mode)

;; auto-fill-mode associ� au text-mode
(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; Ligne courante en surbrillance
(global-hl-line-mode t)

;; (), [] 
(if (>= emacs-major-version 24)
	(electric-pair-mode t)
)

;; Mise en �vidence des parenth�ses et des accolades
(require 'paren)
(show-paren-mode t)
;; parenthesis, expression ou mixed
(setq show-paren-style 'mixed)
(setq blink-matching-paren t)
(setq blink-matching-paren-on-screen t)
(setq blink-matching-paren-dont-ignore-comments t)

;; Titre de la fen�tre
(setq frame-title-format '(buffer-file-name "Emacs - %b" "Emacs - %b"))

;; Active la roulette de la souris
(mwheel-install)

;; Locales syst�me
;;(setq locale-coding-system 'iso-8859-15)
;;(set-terminal-coding-system 'iso-8859-15)
;;(set-keyboard-coding-system 'iso-8859-15)
;;(set-selection-coding-system 'iso-8859-15)
(prefer-coding-system 'iso-8859-15)
;; dead-diaeresis or dead-circumflex is undefined
(load-library "iso-transl")

(set-language-environment "French")

;; R�gle typographique fran�aise
(setq sentence-end-double-space nil)

;; Police dans
(if (>= emacs-major-version 23)
	(set-default-font "Monospace-9")
	;(set-default-font "Droid Sans-9")
)

;; Copier/coller � partir d'un programme X vers Emacs
(setq selection-coding-system 'compound-text-with-extensions)

;; N'interragit pas avec le � clipboard �
(setq x-select-enable-clipboard-manager nil)

;; Ajoute un saut de ligne � la fin des fichiers
(setq-default require-final-newline t)

;; Afficher l'heure
;(display-time)
;(setq display-time-24hr-format t)

;; Suppression des � beep �
(setq visible-bell t)

;; Lancer au d�marrage de la premi�re instance de Emacs
;; emacsclient
;(server-start)

;; Le calendrier
(setq diary-file "~/.diary")
;; Le d�but de la semaine commence le lundi
(setq calendar-week-start-day 1)
(setq european-calendar-style t)
;; Les vacances chr�tiennes
(setq all-christian-calendar-holiday t)
;; Les jours en fran�ais
(defvar calendar-day-abbrev-array
  ["dim" "lun" "mar" "mer" "jeu" "ven" "sam"])
(defvar calendar-day-name-array
  ["Dimanche" "Lundi" "Mardi" "Mercredi" "Jeudi" "Vendredi" "Samedi"])
;; Les mois en fran�ais
(defvar calendar-month-name-array
  ["janvier" "f�vrier" "mars" "avril" "mai" "juin" "juillet"
   "ao�t" "septembre" "octobre" "novembre" "d�cembre"])

;; ======================== clavier ========================
;; Aller directement � une ligne
(global-set-key [(meta g)] 'goto-line)

;; Annuler
(global-set-key [?\C-x u] 'undo)
;; =========================================================

;; ======================= load-path =======================
(if (string= system-type "berkeley-unix")
    ; TODO: voir pour les autres BSD
    (if (string= (getenv "OSTYPE") "FreeBSD")
	(setq lisp-dir "/usr/local/share/emacs/site-lisp/")
      )
  ; Linux
  (setq lisp-dir "/usr/share/emacs/site-lisp/")
)
;; =========================================================

;; ========================= modes =========================
(setq auto-mode-alist
      (append
       (list
	(cons "\\.sh$" 'sh-mode)
	(cons "\\.\\(c\\|mk\\)shrc$" 'sh-mode)
	(cons "\\.xinitrc$" 'sh-mode)
	(cons "\\.profile" 'sh-mode)
	(cons "\\.pl$" 'cperl-mode)
	(cons "\\.py$" 'python-mode) 
	(cons "\\.fcgi$" 'python-mode) 
	(cons "\\.wsgi$" 'python-mode) 
	(cons "\\.el$" 'emacs-lisp-mode)
	(cons "\\.awk$" 'awk-mode)
	(cons "\\.sql$" 'sql-mode)
	(cons "[Mm]\\(akefile\\|akeconf\\)$" 'makefile-mode)
	(cons "[Ii]makefile" 'makefile-mode)
	(cons "\\.mk$" 'makefile-mode)
	(cons "\\.java$" 'java-mode)
	(cons "\\.\\(texi\\|texinfo\\)$" 'texinfo-mode)
	(cons "\\.php$" 'c-mode)
	)
       auto-mode-alist)
      )

;; ---------------- erc-mode ----------------
(if (> emacs-major-version 22)
    (require 'erc)

  (require 'erc-fill)

  (require 'erc-netsplit)
  (erc-netsplit-mode t)

  ;; L'heure
  (erc-timestamp-mode t)
  (setq erc-timestamp-format "[%H:%M] ")

  ;; Pr�fixe pour les longs commentaires
  (setq erc-fill-prefix "      + ")

  ;; Infos
  (setq erc-user-full-name "Olivier Duchateau")

  ;; autojoin -> channels
  ;(setq erc-autojoin-channels-alist
		;'(("freenode.net" "#bsdfrance")))
  ;; connexion automatique
  ;(erc :server "irc.freenode.net" :port 6667 :nick "ptitO")
)
;; ------------------------------------------

;; ---------------- css-mode ----------------
;(when (file-exists-p (concat lisp-dir "css-mode.el"))
 ; (autoload 'css-mode "css-mode")
  
  ;; Indentation fa�on � C style �
  ;(setq cssm-indent-function #'cssm-c-style-indenter)
  
  ;; Charge le mode css pour le fichiers suivants
  ;(add-to-list 'auto-mode-alist 
;			   '("\\.css$" . css-mode))
;)
;; ------------------------------------------

;; --------------- python-mode --------------
(add-hook 'python-mode-hook
	(lambda ()
		(setq indent-tabs-mode t)
		(setq tab-width (default-value 'tab-width))))
;; ------------------------------------------

;; ---------------- sql-mode ----------------
(add-hook 'sql-mode-hook 'font-lock-mode)
;; ------------------------------------------

;; ---------------- java-mode ---------------
(autoload 'java-mode "java mode" t nil)
;; ------------------------------------------

;; ------------------ c-mode ----------------
(setq-default c-basic-offset 4)
;; ------------------------------------------

;; ----------------- vala-mode --------------
(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))
(autoload 'vala-mode "vala-mode"
	"Major mode for editing Vala code." t)
(setq auto-mode-alist
      (append
       (list
	(cons "\\.va\\(la\\|pi\\)$" 'vala-mode))
       auto-mode-alist))
;; ------------------------------------------

;; --------------- graphviz-mode ------------
(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))
(autoload 'graphviz-dot-mode "graphviz-dot-mode"
	"Major mode for editing Graphviz files." t)
(setq auto-mode-alist
      (append
       (list
	(cons "\\.\\(dot\\|gv\\)$" 'graphviz-dot-mode))
       auto-mode-alist))
;; ------------------------------------------

;; --------------- markdown-mode ------------
(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))
(autoload 'markdown-mode "markdown-mode"
	"Major mode for editing Markdown files." t)
(setq auto-mode-alist
      (append
       (list
	(cons "\\.md$" 'markdown-mode))
       auto-mode-alist))
;; ------------------------------------------

;; ---------------- textile-mode ------------
(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))
(autoload 'textile-mode "textile-mode"
	"Major mode for editing Textile files." t)
(setq auto-mode-alist
      (append
       (list
	(cons "\\.textile$" 'textile-mode))
       auto-mode-alist))
;; ------------------------------------------

(defun local-sgml-mode-hook
	(setq fill-column 70
				indent-tabs-mode nil
				next-line-add-newlines nil
				standard-indent 2
				sgml-indent-data t)
	(auto-fill-mode t)
	(setq sgml-catalog-files '("/usr/local/share/xml/catalog")))
(add-hook 'psgml-mode-hook
					'(lambda () (local-psgml-mode-hook)))

(setq nxml-child-indent 2)
