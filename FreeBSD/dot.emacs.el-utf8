;; .emacs.el
;; -*- coding: utf-8 -*-
;;

;; Buffer *scratch* vide au lancement de Emacs
(setq inhibit-startup-message t
	inhibit-startup-echo-area-message t)

;; Affiche que les erreurs importantes
;(setq warning-minimum-level :emergency)

;; Suppression de la barre d'outils (celle avec les icones)
(tool-bar-mode 0)
(menu-bar-mode nil)

;; Suppression de la « scroll » barre
(scroll-bar-mode 0)

;; Empêche le clignotement du curseur
(blink-cursor-mode 0)

;; Désactive la sauvegarde des fichiers (avec l'extension ~)
(setq make-backup-files nil)

;; Affiche le numéro de la ligne
(line-number-mode t)

;; Affiche le numéro de colonne
(column-number-mode t)

;; Affiche la taille du fichier (celui du buffer courant)
(size-indication-mode t)

;; Le contenu se déplace d'une seule ligne
(setq scroll-step 1)

(setq initial-frame-alist
			'(
				(width . 70)
				(height . 35) ; les lignes
))

(setq default-frame-alist
			'(
				(width . 70)
				(height . 35) ; les lignes
))

;; Lignes ne dépassant pas 70 colonnes
(setq-default fill-column 70)

;; Thème
;(load-theme 'leuven)
(load-theme 'misterioso)

;; Tabulation (longueur)
(setq-default tab-width 2)

;; Active la coloration syntaxique
(global-font-lock-mode t)
(setq-default font-lock-maximum-decoration t)

;; Mode majeur text-mode
(setq default-major-mode 'text-mode)

;; auto-fill-mode associé au text-mode
(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; Ligne courante en surbrillance
(global-hl-line-mode t)

;; (), [] 
(if (>= emacs-major-version 24)
	(electric-pair-mode t)
)

;; Mise en évidence des parenthèses et des accolades
(require 'paren)
(show-paren-mode t)
;; parenthesis, expression ou mixed
(setq show-paren-style 'mixed)
(setq blink-matching-paren t)
(setq blink-matching-paren-on-screen t)
(setq blink-matching-paren-dont-ignore-comments t)

;; Titre de la fenêtre
(setq frame-title-format
			'(buffer-file-name "Emacs - %b" "Emacs - %b"))

;; Active la roulette de la souris
(mwheel-install)

;; Locales système
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; Règle typographique française
(setq sentence-end-double-space nil)

;; Police
(add-to-list 'default-frame-alist
						 '(font . "Monospace-10"))
;(if (>= emacs-major-version 23)
;	(set-default-font "Monospace 10")
;)


;; Copier/coller à partir d'un programme X vers Emacs
(setq selection-coding-system 'compound-text-with-extensions)

;; Ajoute un saut de ligne à la fin des fichiers
(setq-default require-final-newline t)

;; Afficher l'heure
;(display-time)
;(setq display-time-24hr-format t)

;; Suppression des « beep »
(setq visible-bell t)

;; Lancer au démarrage de la première instance de Emacs
;; emacsclient
;(server-start)

;; Le calendrier
(setq diary-file "~/.diary")
;; Le début de la semaine commence le lundi
(setq calendar-week-start-day 1)
(setq european-calendar-style t)
;; Les vacances chrétiennes
(setq all-christian-calendar-holiday t)
;; Les jours en français
(defvar calendar-day-abbrev-array
  ["dim" "lun" "mar" "mer" "jeu" "ven" "sam"])
(defvar calendar-day-name-array
  ["Dimanche" "Lundi" "Mardi" "Mercredi" "Jeudi" "Vendredi" "Samedi"])
;; Les mois en français
(defvar calendar-month-name-array
  ["janvier" "février" "mars" "avril" "mai" "juin" "juillet"
   "août" "septembre" "octobre" "novembre" "décembre"])

;; ======================== clavier ========================
;; Aller directement à une ligne
;(global-set-key [(meta g)] 'goto-line)
(global-set-key (kbd "M-g") 'goto-line)

;; Annuler
(global-set-key (kbd "C-z") 'undo)
;; =========================================================

;; ======================= load-path =======================
(if (string= system-type "berkeley-unix")
    ; TODO: voir pour les autres BSD
    (if (string= (getenv "OSTYPE") "FreeBSD")
	(setq lisp-dir "/usr/local/share/emacs/site-lisp/")
      )
  ; Linux
  (setq lisp-dir "/usr/share/emacs/site-lisp/")
)
;; =========================================================

;; ========================= modes =========================
(setq auto-mode-alist
      (append
       (list
	(cons "\\.sh$" 'sh-mode)
	(cons "\\.\\(c\\|mk\\)shrc$" 'sh-mode)
	(cons "\\.xinitrc$" 'sh-mode)
	(cons "\\.profile" 'sh-mode)
	(cons "\\.pl$" 'cperl-mode)
	(cons "\\.py$" 'python-mode) 
	(cons "\\.fcgi$" 'python-mode) 
	(cons "\\.wsgi$" 'python-mode) 
	(cons "\\.el$" 'emacs-lisp-mode)
	(cons "\\.awk$" 'awk-mode)
	(cons "\\.sql$" 'sql-mode)
	(cons "[Mm]\\(akefile\\|akeconf\\)$" 'makefile-mode)
	(cons "[Ii]makefile" 'makefile-mode)
	(cons "\\.mk$" 'makefile-mode)
	(cons "\\.java$" 'java-mode)
	(cons "\\.\\(texi\\|texinfo\\)$" 'texinfo-mode)
	(cons "\\.php$" 'c-mode)
	)
       auto-mode-alist)
      )

;; --------------- css-mode
;(when (file-exists-p (concat lisp-dir "css-mode.el"))
 ; (autoload 'css-mode "css-mode")
  
  ;; Indentation façon « C style »
  ;(setq cssm-indent-function #'cssm-c-style-indenter)
  
  ;; Charge le mode css pour le fichiers suivants
  ;(add-to-list 'auto-mode-alist 
;			   '("\\.css$" . css-mode))
;)
;; ------------------------------------------

;; --------------- python-mode
(add-hook 'python-mode-hook
	(lambda ()
		(setq python-indent-offset 4)))
;; ------------------------------------------

;; --------------- sql-mode
(add-hook 'sql-mode-hook 'font-lock-mode)
;; ------------------------------------------

;; --------------- java-mode
(autoload 'java-mode "java mode" t nil)
;; ------------------------------------------

;; --------------- c-mode
(setq-default c-default-style "bsd"
							c-basic-offset 2)
;; ------------------------------------------

;; --------------- vala-mode
(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))
(autoload 'vala-mode "vala-mode"
	"Major mode for editing Vala code." t)
(setq auto-mode-alist
      (append
       (list
	(cons "\\.va\\(la\\|pi\\)$" 'vala-mode))
       auto-mode-alist))
(add-to-list 'file-coding-system-alist '("\\.vala$" . utf-8))
(add-to-list 'file-coding-system-alist '("\\.vapi$" . utf-8))
;; ------------------------------------------

;; --------------- cmake-mode
;(add-to-list 'load-path (expand-file-name "~/.emacs.d"))
;(require 'cmake-mode)
;(setq auto-mode-alist
;			(append '(("CMakeLists\\.txt\\'" . cmake-mode)
;								("\\.cmake\\'" . cmake-mode))
;							auto-mode-alist))
;; ------------------------------------------

;; --------------- scss-mode
;(add-to-list 'load-path (expand-file-name "~/.emacs.d"))
;(autoload 'scss-mode "scss-mode")
;(add-to-list 'auto-mode-alist '("\\.scss\\'" . scss-mode))

;(setq exec-path (cons (expand-file-name "/usr/bin/") exec-path))
;; ------------------------------------------

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(css-indent-offset 2))
