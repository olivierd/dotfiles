" .vimrc
"

set fileencodings=utf-8,iso-8859-15,iso-8859-1

set showmatch
set showmode
set ruler
"set number
syntax on
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab

set smartindent

" Pas de sauvegarde
set nobackup
set nowritebackup
set noswapfile

"set cursorline

" Pas de ~/.viminfo
set vi=""
