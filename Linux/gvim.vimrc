" .vimrc
"

set fileencodings=utf-8,iso-8859-15,iso-8859-1

set showmatch
set showmode
set ruler
syntax on
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab

"set smartindent
"set cindent

"set list
"set listchars=tab:⋅−

" Pas de sauvegarde
set nobackup
set nowritebackup
set noswapfile

" On cache la toolbar
if has("gui_running")
	set cursorline
	set number

	set ts=4
	set sw=4
	set guioptions-=T
	"set guioptions-=m

    " Évite les bips ennuyeux
    autocmd GUIEnter * set vb t_vb=

	if has("gui_gtk2") || has("gui_gtk3")
		set guifont=Monospace\ 10
	endif

	set lines=40 columns=80

	" Vala support
	let vala_comment_strings = 1
endif

autocmd BufNewFile,BufRead *.policy set syntax=xml
autocmd BufNewFile,BufRead *.rules set syntax=JavaScript

" Pas de ~/.viminfo
set vi=""
